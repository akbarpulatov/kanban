import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {

  group('Kanban Card model test', (){

    test('Combination test to Json and from Json', (){
      final _val = KanbanCard.fromJson(json.decode(fixture('kanban-card.json')));
      final _valParsed = KanbanCard.fromJson(json.decode(jsonEncode(_val.toJson())));
      expect(_val, _valParsed);
    });

  });

  group('Cards model test', (){

    test('Combination test to Json and from Json', (){
      final _val = Cards.fromJsonList(json.decode(fixture('cards.json')));
      final _valParsed = Cards.fromJsonList(json.decode(jsonEncode(_val.toJsonList())));
      expect(_val, _valParsed);
    });

  });
}