import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:kanban_test/features/auth/domain/entities/user.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  const tUser = User(
      userName: 'username',
      email: 'user@example.com',
      password: 'password'
  );

  group('Test model methods', (){
    test('from Json', () async {

      final _parsedUser = User.fromJson(jsonDecode(fixture('user.json')));
      expect(tUser, _parsedUser);

    });

    test('Combination', (){
      final _val = User.fromJson(json.decode(fixture('user.json')));
      final _valParsed = User.fromJson(json.decode(jsonEncode(_val.toJson())));
      expect(_val, _valParsed);
    });

  });
}