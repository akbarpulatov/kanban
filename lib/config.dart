abstract class Config {
  String get trueApiBaseUrl;
}

class TestConfig extends Config {
  @override
  String get trueApiBaseUrl =>
      const String.fromEnvironment('TRUEAPI_BASE_URL', defaultValue: 'https://trello.backend.tests.nekidaem.ru/api/v1');
}

class ProdConfig extends Config {
  @override
  String get trueApiBaseUrl =>
      const String.fromEnvironment('TRUEAPI_BASE_URL', defaultValue: 'https://trello.backend.tests.nekidaem.ru/api/v1');
}