import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as st;
import 'package:kanban_test/features/auth/domain/repositories/auth_repository.dart';
import 'package:kanban_test/features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'package:kanban_test/service_locator.dart';

class NetworkClient {
  static const _tokenKey = 'token-key';
  String _token = '';
  late final Dio api;
  static int _retryCount = 0;
  static const _maxRetryCount = 10;

  Future<Dio> init(st.FlutterSecureStorage storage) async {
    api = Dio();
    _token = await storage.read(key: _tokenKey) ?? '';
    api.interceptors.add(InterceptorsWrapper(

        /// onRequest
        onRequest: (options, handler) async {
          if (_token != '') {
            options.headers['Authorization'] = 'JWT $_token';
          }
          return handler.next(options);
        },

      /// onError
      onError: (e, handler) async {
        _retryCount ++;

        if (e.response?.statusCode == 401 || e.response?.statusCode == 403) {
          if(_retryCount > _maxRetryCount){
            _goToLoginScreen();
          }

          final _result = await sl<AuthRepository>().reAuth();
          _result.fold(
              (failure){},
              (token){
                _token = token;
              }
          );

            final opts = Options(
                method: e.requestOptions.method,
                headers: e.requestOptions.headers);

            final cloneReq = await api.request(e.requestOptions.path,
                options: opts,
                data: e.requestOptions.data,
                queryParameters: e.requestOptions.queryParameters);

            return handler.resolve(cloneReq);
        }
        return handler.next(e);
      },

    ));
    return api;
  }

  _refreshToken() async {
    final response = await sl<AuthRepository>().refreshToken();
    response.fold(
            (failure) => _goToLoginScreen(),
            (refreshedToken){
          _token = refreshedToken;
        }
    );
  }

  _goToLoginScreen(){
    sl<AuthCubit>().unAuth();
  }

}
