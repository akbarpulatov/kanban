import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

///***************************************************
/// Core Failures
///***************************************************

class CacheFailure extends Failure {
  @override
  String toString() => 'Database Failure';
}

class ServerFailure extends Failure {}

///***************************************************
/// Auth Failures
///***************************************************

class AuthFailure extends Failure {
  final String text;

  AuthFailure(this.text);
}

class TokenNotFound extends Failure {}
