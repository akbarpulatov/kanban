import 'dart:convert';

import 'package:equatable/equatable.dart';

enum StatusRow { onHold, inProgress, needsReview, approved }

Cards cardsFromJson(String str) => Cards.fromJsonList(json.decode(str));

String cardsToJson(Cards data) => json.encode(data.toJsonList());

class Cards extends Equatable {
  final List<KanbanCard> cardsData;

  const Cards(this.cardsData);

  factory Cards.fromJsonList(Iterable jsonList) {
    return Cards(
      List<KanbanCard>.from(jsonList.map((model) => KanbanCard.fromJson(model)))
    );
  }

  Iterable toJsonList() {
    return List<dynamic>.from(cardsData.map((model) => model.toJson()));
  }

  @override
  List<Object?> get props => [cardsData];

}

KanbanCard kanbanCardFromJson(String str) => KanbanCard.fromJson(json.decode(str));

String kanbanCardToJson(KanbanCard data) => json.encode(data.toJson());

class KanbanCard extends Equatable {
  final int? id;
  final StatusRow row;
  final int? seqNum;
  final String text;

  const KanbanCard({
    this.id,
    required this.row,
    this.seqNum,
    required this.text
  });

  @override
  List<Object?> get props => [id, row, seqNum, text];

  factory KanbanCard.fromJson(Map<String, dynamic> json) {

    return KanbanCard(
      id: json['id'],
      row: (json['row'] as String).toStatusRow(),
      seqNum: json['seq_num'],
      text: json['text'],
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'row': row.toRowString(),
    'seq_num': seqNum,
    'text': text,
  };

}

extension ToRowString on StatusRow {
  String toRowString(){
    switch(this) {
      case StatusRow.onHold: return '0';
      case StatusRow.inProgress: return '1';
      case StatusRow.needsReview: return '2';
      case StatusRow.approved: return '3';
      default: return '';
    }
  }
}

extension ToStatusRow on String {
  StatusRow toStatusRow(){
    switch(this) {
      case '0': return StatusRow.onHold;
      case '1': return StatusRow.inProgress;
      case '2': return StatusRow.needsReview;
      case '3': return StatusRow.approved;
      default: return StatusRow.onHold;
    }
  }
}
