import 'package:dartz/dartz.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

abstract class DashboardRepository {
  Future<Either<Failure, Cards>> getCards(StatusRow? row);
  Future<Either<Failure, KanbanCard>> createCard(KanbanCard card);
  Future<Either<Failure, KanbanCard>> updateCard(KanbanCard card);
  Future<Either<Failure, bool>> deleteCard(int id);
}
