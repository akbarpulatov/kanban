import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';
import 'package:kanban_test/features/dashboard/domain/repositories/dashboard_repository.dart';

part 'row_items_state.dart';

class RowItemsCubit extends Cubit<RowItemsState> {
  final DashboardRepository _dashboardRepository;
  RowItemsCubit(this._dashboardRepository) : super(RowItemsInitial());

  Future<void> loadRowItems(StatusRow? row) async {
    emit(RowItemsLoading());
    final dashbaordOrFailure = await _dashboardRepository.getCards(row);
    emit(
      dashbaordOrFailure.fold(
        (failure) => RowItemsError(failure.toString()),
        (cards) => RowItemsLoaded(cards: cards),
      ),
    );
  }
}
