part of 'row_items_cubit.dart';

abstract class RowItemsState extends Equatable {
  const RowItemsState();

  @override
  List<Object> get props => [];
}

class RowItemsInitial extends RowItemsState {}

class RowItemsLoading extends RowItemsState {}

class RowItemsLoaded extends RowItemsState {
  final Cards cards;

  const RowItemsLoaded({required this.cards});

  @override
  List<Object> get props => [cards];
}

class RowItemsError extends RowItemsState {
  final String message;
  const RowItemsError(this.message);
  @override
  List<Object> get props => [message];
}
