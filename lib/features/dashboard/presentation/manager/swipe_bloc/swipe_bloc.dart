import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

part 'swipe_event.dart';
part 'swipe_state.dart';

class SwipeBloc extends Bloc<SwipeEvent, SwipeState> {
  SwipeBloc() : super(SwipeLoading()) {
    on<LoadCardsEvent>(_onLoadCardEvent);
    on<SwipeLeftEvent>(_onSwipeLeftEvent);
    on<SwipeLeftEvent>(_onSwipeRightEvent);
  }

  FutureOr<void> _onLoadCardEvent(LoadCardsEvent event, Emitter<SwipeState> emit) {
    emit(SwipeLoaded(cards: event.cards));
  }

  FutureOr<void> _onSwipeLeftEvent(SwipeLeftEvent event, Emitter<SwipeState> emit) {
    if (state is SwipeLoaded) {
      try {
        emit(SwipeLoaded(cards: (state as SwipeLoaded).cards));
      } catch (_) {}
    }
  }

  FutureOr<void> _onSwipeRightEvent(SwipeLeftEvent event, Emitter<SwipeState> emit) {}
}
