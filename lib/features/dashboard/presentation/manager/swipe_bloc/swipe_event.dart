part of 'swipe_bloc.dart';

abstract class SwipeEvent extends Equatable {
  const SwipeEvent();
}

class LoadCardsEvent extends SwipeEvent {
  final List<KanbanCard> cards;

  const LoadCardsEvent({required this.cards});
  @override
  List<Object?> get props => [cards];
}

class SwipeLeftEvent extends SwipeEvent {
  final List<KanbanCard> cards;

  const SwipeLeftEvent({required this.cards});
  @override
  List<Object?> get props => [cards];
}

class SwipeRightEvent extends SwipeEvent {
  final List<KanbanCard> cards;

  const SwipeRightEvent({required this.cards});

  @override
  List<Object?> get props => [cards];
}
