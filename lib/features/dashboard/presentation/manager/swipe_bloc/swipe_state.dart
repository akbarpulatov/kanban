part of 'swipe_bloc.dart';

abstract class SwipeState extends Equatable {
  const SwipeState();
  @override
  List<Object?> get props => [];
}

class SwipeLoading extends SwipeState {}

class SwipeLoaded extends SwipeState {
  final List<KanbanCard> cards;

  const SwipeLoaded({required this.cards});

  @override
  List<Object?> get props => [cards];
}

class SwipeError extends SwipeState {}
