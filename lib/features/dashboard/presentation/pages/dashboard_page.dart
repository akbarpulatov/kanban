import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kanban_test/features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'package:kanban_test/features/auth/presentation/pages/login_page.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';
import 'package:kanban_test/features/dashboard/presentation/widgets/nav_bar.dart';
import 'package:kanban_test/features/dashboard/presentation/widgets/row_status_view_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 4,
        child: BlocListener<AuthCubit, AuthState>(
          listener: (BuildContext context, state) async {
            if (state is NotAuthenticated) {
              await Navigator.of(context).pushReplacement(MaterialPageRoute(builder:
                  (context) => const LoginPage()));
            }
          },
          child: Scaffold(
            body: Column(
              children: const [
                NavBar(),
                Flexible(
                  child: TabBarView(
                    children: [
                      RowStatusView(StatusRow.onHold),
                      RowStatusView(StatusRow.inProgress),
                      RowStatusView(StatusRow.needsReview),
                      RowStatusView(StatusRow.approved),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
