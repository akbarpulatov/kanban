import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

class RowCard extends StatelessWidget {
  final KanbanCard card;
  const RowCard({Key? key, required this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.95,
        child: Container(
          decoration: BoxDecoration(
            color: const Color(0xff424242),
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 4,
                spreadRadius: 4,
                offset: const Offset(2, 2),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('ID: ${card.id}',
                    style: const TextStyle(color: Colors.white, fontSize: 15, decoration: TextDecoration.none)),
                const SizedBox(height: 8),
                Text(card.text,
                    style: const TextStyle(color: Colors.white, fontSize: 15, decoration: TextDecoration.none)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
