import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';
import 'package:kanban_test/features/dashboard/presentation/manager/row_items/row_items_cubit.dart';
import 'package:kanban_test/features/dashboard/presentation/widgets/draggable_card_widget.dart';
import 'package:kanban_test/service_locator.dart';

class RowStatusView extends StatelessWidget {
  const RowStatusView(this.row, {Key? key}) : super(key: key);

  final StatusRow row;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<RowItemsCubit>()..loadRowItems(row),
      child: BlocBuilder<RowItemsCubit, RowItemsState>(
        builder: (context, state) {
          if (state is RowItemsInitial) {
          } else if (state is RowItemsLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is RowItemsError) {
            return Center(child: Text(state.message));
          } else if (state is RowItemsLoaded) {
            return ListView.builder(
              itemCount: state.cards.cardsData.length,
              itemBuilder: (context, index) {
                final topCard = state.cards.cardsData[index];
                return DraggableCardWidget(topCard: topCard);
              },
            );
          } else {
            return const CircularProgressIndicator();
          }
          return Container(); //In case none of the if statements is true
        },
      ),
    );
  }
}
