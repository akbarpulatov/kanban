import 'package:flutter/material.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';
import 'package:kanban_test/features/dashboard/presentation/widgets/row_card.dart';

class DraggableCardWidget extends StatelessWidget {
  const DraggableCardWidget({
    Key? key,
    required this.topCard,
  }) : super(key: key);

  final KanbanCard topCard;

  @override
  Widget build(BuildContext context) {
    return Draggable(
      child: RowCard(card: topCard),
      feedback: RowCard(card: topCard),
      axis: Axis.horizontal,
      affinity: Axis.horizontal,
      // childWhenDragging: RowCard(card: topCard),
      onDragEnd: (drag) {
        if (drag.velocity.pixelsPerSecond.dx < 0) {
          print('Swiped to move/update to card to left');
        } else {
          print('Swiped to move/update to card to right');
        }
      },
    );
  }
}
