import 'package:flutter/material.dart';
import 'package:kanban_test/features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'package:provider/src/provider.dart';

class NavBar extends StatelessWidget {
  const NavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 130,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          decoration: BoxDecoration(
            color: const Color(0xff222222),
            borderRadius: BorderRadius.circular(4),
          ),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: FloatingActionButton(
                  backgroundColor: const Color(0xff63ffda),
                  onPressed: () {
                    context.read<AuthCubit>().unAuth();
                  },
                  tooltip: 'Log Out',
                  child: const Icon(
                    Icons.arrow_back,
                  ),
                ),
              ),
              const SizedBox(height: 6),
              const TabBar(
                // controller: controller,
                tabs: [
                  Tab(
                    text: 'On Hold',
                  ),
                  Tab(text: 'In Progress'),
                  Tab(text: 'Needs Review'),
                  Tab(text: 'Approved'),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
