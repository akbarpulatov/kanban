import 'package:hive_flutter/hive_flutter.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

abstract class DashboardLocalDataSource {
  Cards getCards({StatusRow? row});

  Future<void> saveCards(Cards cards, {StatusRow? row});

  /// Returns [KanbanCard] or throws [NetworkException]
  Future<KanbanCard> createCard(KanbanCard card);

  /// Returns [KanbanCard] or throws [NetworkException]
  Future<KanbanCard> updateCard(KanbanCard card);

  /// Delete Card by [id], or throws [NetworkException]
  Future<bool> deleteCard(int id);
}

class DashboardLocalDataSourceImpl implements DashboardLocalDataSource {
  final Box<String> _box;
  DashboardLocalDataSourceImpl(this._box);

  static const Map<StatusRow, String> _map =  {
    StatusRow.onHold: 'on-hold-cards-key',
    StatusRow.inProgress: 'in-progress-cards-key',
    StatusRow.needsReview: 'needs-review-cards-key',
    StatusRow.approved: 'approved-cards-key',
  };


  _updateCards(Cards cards){}

  @override
  Future<void> saveCards(Cards cards, {StatusRow? row}) async {
    _box.put(_map[row], cardsToJson(cards));
  }


  @override
  Cards getCards({StatusRow? row}) {

    final raw = _box.get(_map[row]);
    if(raw != null) {
      final _cards = cardsFromJson(raw);
      return _cards;
    } else {
      return const Cards([]);
    }
  }

  @override
  Future<KanbanCard> createCard(KanbanCard card) async {
    // TODO: implement createCard
    throw UnimplementedError();
  }

  @override
  Future<KanbanCard> updateCard(KanbanCard card) async {
    // TODO: implement updateCard
    throw UnimplementedError();
  }

  @override
  Future<bool> deleteCard(int id) {
    // TODO: implement deleteCard
    throw UnimplementedError();
  }

}