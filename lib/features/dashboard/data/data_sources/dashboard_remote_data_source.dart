import 'package:dio/dio.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';

abstract class DashboardRemoteDataSource {
  /// Return list of the user cards wrapped sorted by row and seq_num
  /// Returns [Cards] or throws [NetworkException]
  Future<Cards> getCards({StatusRow? row});

  /// Returns [KanbanCard] or throws [NetworkException]
  Future<KanbanCard> createCard(KanbanCard card);

  /// Returns [KanbanCard] or throws [NetworkException]
  Future<KanbanCard> updateCard(KanbanCard card);

  /// Delete Card by [id], or throws [NetworkException]
  Future<bool> deleteCard(int id);
}

class DashboardRemoteDataSourceImpl implements DashboardRemoteDataSource {
  final Dio _dio;

  DashboardRemoteDataSourceImpl(this._dio);

  @override
  Future<Cards> getCards({StatusRow? row}) async {
    final response = await _dio.get('https://trello.backend.tests.nekidaem.ru/api/v1/cards/?row=${row!.index}');
    if (response.statusCode == 200) {
      return Cards.fromJsonList(response.data);
    } else {
      throw NetworkException();
    }
  }

  @override
  Future<KanbanCard> createCard(KanbanCard card) {
    // TODO: implement createCard
    throw UnimplementedError();
  }

  @override
  Future<KanbanCard> updateCard(KanbanCard card) {
    // TODO: implement updateCard
    throw UnimplementedError();
  }

  @override
  Future<bool> deleteCard(int id) {
    // TODO: implement deleteCard
    throw UnimplementedError();
  }
}
