import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/dashboard/data/data_sources/dashboard_local_data_source.dart';
import 'package:kanban_test/features/dashboard/data/data_sources/dashboard_remote_data_source.dart';
import 'package:kanban_test/features/dashboard/domain/entities/kanban_card.dart';
import 'package:kanban_test/features/dashboard/domain/repositories/dashboard_repository.dart';

class DashboardRepositoryImpl implements DashboardRepository {
  final DashboardRemoteDataSource remoteDataSource;
  final DashboardLocalDataSource localDataSource;

  DashboardRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
  });

  @override
  Future<Either<Failure, KanbanCard>> createCard(KanbanCard card) {
    // TODO: implement createCard
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, bool>> deleteCard(int id) {
    // TODO: implement deleteCard
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, Cards>> getCards(StatusRow? row) async {
    try {
      final cards = await remoteDataSource.getCards(row: row);
      localDataSource.saveCards(cards, row: row);
      return Right(cards);
    } on DioError {
      final cards = localDataSource.getCards(row: row);
      return Right(cards);
    } catch(e) {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, KanbanCard>> updateCard(KanbanCard card) {
    // TODO: implement updateCard
    throw UnimplementedError();
  }
}
