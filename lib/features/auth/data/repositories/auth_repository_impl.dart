import 'package:dartz/dartz.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/auth/data/data_sources/auth_local_data_source.dart';
import 'package:kanban_test/features/auth/data/data_sources/auth_remote_data_source.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';
import 'package:kanban_test/features/auth/domain/repositories/auth_repository.dart';

class AuthRepositoryImpl extends AuthRepository {

  final AuthLocalDataSource localDataSource;
  final AuthRemoteDataSource remoteDataSource;

  AuthRepositoryImpl({
    required this.localDataSource,
    required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, String>> auth(Login credentials) async {
    try {
      final _token = await remoteDataSource.auth(credentials);
      localDataSource.saveCredentials(credentials);
      localDataSource.saveToken(_token);
      return Right(_token);
    } on AuthException catch(e) {
      return Left(AuthFailure(e.message!));
    }
  }

  @override
  Future<Either<Failure, bool>> unAuth() async {
    try {
      await localDataSource.deleteToken();
      return const Right(true);
    } catch (e) {
      return Left(AuthFailure(e.runtimeType.toString()));
    }
  }

  @override
  Future<Either<Failure, bool>> checkAuth() async {
    try {
      final _checkAuth = await localDataSource.checkAuth();
      return Right(_checkAuth);
    } catch (e) {
      return Left(AuthFailure(e.runtimeType.toString()));
    }
  }

  @override
  Future<Either<Failure, String>> refreshToken() async {
    try {
      final token = await localDataSource.getToken();
      final refreshedToken = await remoteDataSource.refreshToken(token);
      localDataSource.saveToken(refreshedToken);
      return Right(refreshedToken);
    } catch (e) {
      return Left(AuthFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, String>> reAuth() async {
    try {
      final _credentials = await localDataSource.getCredentials();
      final _token = await remoteDataSource.auth(_credentials);
      await localDataSource.saveToken(_token);
      return Right(_token);
    } catch (e) {
      return Left(AuthFailure(e.runtimeType.toString()));
    }
  }

  @override
  Future<Either<Failure, String>> getToken() async {
    try {
      final token = await localDataSource.getToken();
      return Right(token);
    } catch(e) {
      return Left(CacheFailure());
    }
  }

}