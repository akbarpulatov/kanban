import 'package:dartz/dartz.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/auth/data/data_sources/user_local_data_source.dart';
import 'package:kanban_test/features/auth/domain/entities/user.dart';
import 'package:kanban_test/features/auth/domain/repositories/user_repository.dart';

class UserRepositoryImpl extends UserRepository {

  final UserLocalDataSource localDataSource;

  UserRepositoryImpl({required this.localDataSource});

  @override
  Future<Either<Failure, User>> getUser() async {
    try {
      final user = await localDataSource.getUser();
      return Right(user);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> removeUser() async {
    try {
      await localDataSource.deleteUser();
      return const Right(true);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> save(User user) async {
    try {
      await localDataSource.save(user);
      return const Right(true);
    } on CacheException {
      return Left(CacheFailure());
    }
  }}