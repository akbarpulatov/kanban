import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';

abstract class AuthLocalDataSource {

  /// returns String as token or throws [CacheException]
  Future<String> getToken();

  /// returns String as token or throws [CacheException], [TokenNotFoundException]
  Future<bool>saveToken(String token);

  /// returns [bool] success or throws [CacheException]
  Future<bool>deleteToken();

  Future<bool> checkAuth();

  Future<Login> getCredentials();

  Future<void> saveCredentials(Login credentials);
}

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  static const _tokenKey = 'token-key';
  static const _credentialsKey = 'credentials-key';

  final FlutterSecureStorage _storage;

  AuthLocalDataSourceImpl(this._storage);

  @override
  Future<bool> deleteToken() async {
    try {
      await _storage.deleteAll();
      return true;
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<String> getToken() async {

    try {
      final token = await _storage.read(key: _tokenKey);
      if(token != null) {
        return token;
      } else {
        throw TokenNotFoundException();
      }
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<bool> saveToken(String token) async {
    try {
      await _storage.write(key: _tokenKey, value: token);
      return true;
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<bool> checkAuth() async {
    final result = await _storage.containsKey(key: _tokenKey);
    return result;
  }

  @override
  Future<Login> getCredentials() async {
    final _credentialsRaw = await _storage.read(key: _credentialsKey);
    if(_credentialsRaw != null){
      final _credentials = userFromJson(_credentialsRaw);
      return _credentials;
    } else {
      throw CacheException();
    }

  }

  @override
  Future<void> saveCredentials(Login credentials) => _storage.write(key: _credentialsKey, value: userToJson(credentials));

}