import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';
import 'package:kanban_test/features/auth/domain/entities/user.dart';

abstract class AuthRemoteDataSource {

  Future<String> createUser(User user);

  /// tries to auth, returns [AuthResult]
  /// throws [AuthException]
  Future<String> auth(Login credentials);

  /// Refresh Token
  Future<String> refreshToken(String token);

}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {

  final Dio _dio;

  AuthRemoteDataSourceImpl(this._dio);

  @override
  Future<String> createUser(User user) {
    // TODO: implement createUser
    throw UnimplementedError();
  }

  @override
  Future<String> auth(Login credentials) async {

    try {
      final response = await _dio.post('https://trello.backend.tests.nekidaem.ru/api/v1/users/login/',
          data: jsonEncode(credentials.toMap())
      );
      if(response.statusCode == 200) {
        return response.data['token'];
      } else {
        throw AuthException();
      }
    } on DioError catch (e) {
      final statusCode = e.response?.statusCode;
      if(statusCode == 400){
        throw AuthException('Unable to log in with provided credentials.');
      } else {
        throw AuthException('something went wrong');
      }
    } on TimeoutException {
      throw AuthException('timeout exception');
    }
  }

  @override
  Future<String> refreshToken(String token) async {
    final dio = Dio();
    final response = await dio.post('https://trello.backend.tests.nekidaem.ru/api/v1/users/refresh_token/',
      data: jsonEncode({ 'token': token })
    );
    if(response.statusCode == 200){
      return response.data['token'];
    } else {
      throw AuthException();
    }
  }

}