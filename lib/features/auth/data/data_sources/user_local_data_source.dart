import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kanban_test/core/error/exceptions.dart';
import 'package:kanban_test/features/auth/domain/entities/user.dart';

abstract class UserLocalDataSource {

  /// gets [User] from Local Storage
  /// throws [CacheException]
  Future<User> getUser();

  /// deletes user info from local storage
  /// throws [CacheException]
  Future<void> deleteUser();

  Future<void> save(User user);
}

class UserLocalDataSourceImpl implements UserLocalDataSource {
  static const _userKey = 'user-key';

  final FlutterSecureStorage _storage;

  UserLocalDataSourceImpl(this._storage);

  @override
  Future<User> getUser() async {
    final _userRaw = await _storage.read(key: _userKey);
    if(_userRaw != null){
      return userFromJson(_userRaw);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> deleteUser() => _storage.delete(key: _userKey);

  @override
  Future<void> save(User user) => _storage.write(key: _userKey, value: userToJson(user));

}