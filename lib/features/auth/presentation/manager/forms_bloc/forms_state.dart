part of 'forms_bloc.dart';

@immutable
class FormsState extends Equatable {
  const FormsState({
        required this.isUserNameValid,
        required this.isPasswordValid,
        required this.isSubmitting,
        required this.isSuccess,
        required this.isFailure
      });

  final bool isUserNameValid;
  final bool isPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;

  bool get isFormValid =>
      isUserNameValid && isPasswordValid;

  factory FormsState.empty() {
    return const FormsState(
        isUserNameValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
    );
  }

  factory FormsState.loading() {
    return const FormsState(
      isUserNameValid: true,
      isPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory FormsState.failure() {
    return const FormsState(
      isUserNameValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory FormsState.success() {
    return const FormsState(
      isUserNameValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }


  FormsState update({
    bool? isUserNameValid,
    bool? isPasswordValid,
  }){
    return copyWith(
      isUserNameValid: isUserNameValid,
      isPasswordValid: isPasswordValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  FormsState copyWith({
    bool? isUserNameValid,
    bool? isPasswordValid,
    bool? isSubmitting,
    bool? isSuccess,
    bool? isFailure,
  }){
    return FormsState(
        isUserNameValid: isUserNameValid ?? this.isUserNameValid,
        isPasswordValid: isPasswordValid ?? this.isPasswordValid,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailure ?? this.isFailure
    );
  }

  @override
  List<Object?> get props => [
    isUserNameValid,
    isPasswordValid,
    isSubmitting,
    isSuccess,
    isFailure,
  ];
}
