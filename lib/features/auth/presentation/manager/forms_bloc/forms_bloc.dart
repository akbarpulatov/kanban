import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:kanban_test/core/utils/validators.dart';

part 'forms_event.dart';
part 'forms_state.dart';

class FormsBloc extends Bloc<FormsEvent, FormsState> {
  FormsBloc() : super(FormsState.empty());

  @override
  Stream<FormsState> mapEventToState(FormsEvent event) async* {
    if(event is NameChanged) {
      yield* _nameChanged(event.name);
    } else if(event is PasswordChanged){
      yield* _passwordChanged(event.password);
    }
  }

  Stream<FormsState> _nameChanged(String name) async* {
    yield state.update(
      isUserNameValid: Validators.isValidName(name)
    );
  }

  Stream<FormsState> _passwordChanged(String password) async* {
    yield state.update(
        isPasswordValid: Validators.isValidPassword(password)
    );
  }


}
