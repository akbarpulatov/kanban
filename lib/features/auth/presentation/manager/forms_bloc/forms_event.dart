part of 'forms_bloc.dart';

abstract class FormsEvent extends Equatable {
  const FormsEvent();

  @override
  List<Object?> get props => [];
}

class NameChanged extends FormsEvent {
  final String name;
  const NameChanged(this.name);
}

class PasswordChanged extends FormsEvent {
  final String password;
  const PasswordChanged(this.password);
}

class Submitted extends FormsEvent {}

