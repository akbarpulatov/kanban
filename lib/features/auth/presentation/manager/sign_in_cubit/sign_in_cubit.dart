import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';
import 'package:kanban_test/features/auth/domain/repositories/auth_repository.dart';

part 'sign_in_state.dart';

class SignInCubit extends Cubit<SignInState> {
  final AuthRepository repository;


  SignInCubit(this.repository) : super(SignInInitial());

  auth(Login credentials) async {
    emit(SignInLoading());
    final result = await repository.auth(credentials);
    result.fold(
        (failure) {
          emit(SignInError((failure as AuthFailure).text));
        },
        (token) {
          emit(SignInSuccessfulState());
        }
    );
    emit(SignInLoading());

  }

}
