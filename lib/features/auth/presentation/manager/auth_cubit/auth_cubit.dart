import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kanban_test/features/auth/domain/repositories/auth_repository.dart';
import 'package:kanban_test/features/auth/domain/repositories/user_repository.dart';
import 'package:kanban_test/features/auth/presentation/manager/sign_in_cubit/sign_in_cubit.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {

  late StreamSubscription _signInSub;
  final UserRepository userRepo;
  final AuthRepository authRepository;
  final SignInCubit signInCubit;
  AuthCubit({required this.authRepository, required this.signInCubit, required this.userRepo}) : super(AuthInitial()){

    _signInSub = signInCubit.stream.listen(_signInListener);

    authRepository.checkAuth().then((value){
      value.fold(
        (failure) => null,
        (isAuth){
          if(isAuth){
            emit(Authenticated());
          } else {
            emit(NotAuthenticated());
          }
        });
    });

  }

  Future<void> unAuth() async {
    await authRepository.unAuth();
    emit(NotAuthenticated());
  }

  Future<void> _signInListener(SignInState signInstate) async {
    if(signInstate is SignInSuccessfulState){
      return emit(Authenticated());
    }
    return;
  }

  @override
  Future<void> close() {
    _signInSub.cancel();
    return super.close();
  }
}
