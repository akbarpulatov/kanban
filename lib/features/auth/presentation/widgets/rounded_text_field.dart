import 'package:flutter/material.dart';

class RoundedTextField extends StatelessWidget {

  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final String? hintText;
  final bool obscureText;
  final FocusNode? focusNode;

  const RoundedTextField({Key? key,
    this.controller,
    required this.validator,
    this.obscureText = false,
    this.hintText,
    this.focusNode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: controller,
      textAlign: TextAlign.center,
      obscureText: obscureText,
      decoration: InputDecoration(
        hintText: hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        filled: true,
      ),
      keyboardType: TextInputType.text,
      autocorrect: false,
      validator: validator,
    );
  }
}
