import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RoundedAnimatedButton extends StatelessWidget {
  final ButtonStatus buttonStatus;
  final VoidCallback? onPressed;
  final String? label;

  /// onPressed is triggered only if buttonStatus is ENABLED
  const RoundedAnimatedButton({Key? key, this.label, this.onPressed, this.buttonStatus = ButtonStatus.enabled}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      height: 50,
      child: TextButton(
          child: buttonStatus != ButtonStatus.loading
              ? Text(label ?? '', )
              : const CupertinoActivityIndicator(radius: 17),
          onPressed: buttonStatus == ButtonStatus.enabled ? onPressed : null,
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(buttonStatus != ButtonStatus.disabled
                  ? Theme.of(context).cardColor : Theme.of(context).disabledColor),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  )
              )
          )
      ),
    );
  }
}

enum ButtonStatus {
  enabled,
  disabled,
  loading,
}


