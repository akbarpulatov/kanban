import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';
import 'package:kanban_test/features/auth/presentation/manager/forms_bloc/forms_bloc.dart';
import 'package:kanban_test/features/auth/presentation/manager/sign_in_cubit/sign_in_cubit.dart';
import 'package:kanban_test/features/auth/presentation/widgets/rounded_animated_button.dart';
import 'package:kanban_test/features/auth/presentation/widgets/rounded_text_field.dart';
import 'package:kanban_test/features/dashboard/presentation/pages/dashboard_page.dart';
import 'package:kanban_test/generated/l10n.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _nameFocusNode = FocusNode();

  late final FormsBloc _loginBloc;
  late final SignInCubit _signInCubit;

  @override
  void initState() {
    super.initState();
    _setFocusToName();
    _loginBloc = context.read<FormsBloc>();
    _signInCubit = context.read<SignInCubit>();
    _nameController.addListener(_onNameChanged);
    _passwordController.addListener(onPasswordChanged);
  }

  /// Call to set Focus to Name Field
  void _setFocusToName(){
    _nameFocusNode.requestFocus();
  }

  /// Called whenever Name field is changed
  void _onNameChanged() {
    _loginBloc.add(NameChanged(_nameController.text));
  }

  /// Called whenever Password field is changed
  void onPasswordChanged() {
    _loginBloc.add(PasswordChanged(_passwordController.text));
  }

  /// shows SnackBar
  void _showInSnackBar(String message) {
    ScaffoldMessenger.of(context).clearSnackBars();
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  bool get _isRegisterButtonEnabled {
    return _loginBloc.state.isFormValid &&
        isPopulated &&
        !_loginBloc.state.isSubmitting;
  }

  bool get isPopulated =>
      _passwordController.text.isNotEmpty && _nameController.text.isNotEmpty;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(S.of(context).kanban),
        centerTitle: false,
      ),
      body: BlocListener<SignInCubit, SignInState>(
        listener: (context, state) async {
          if(state is SignInSuccessfulState) {
            _nameController.clear();
            _passwordController.clear();
            await Navigator.of(context).pushReplacement(MaterialPageRoute(builder:
                (context) => const DashboardPage()));
          } else if(state is SignInError) {
            _passwordController.clear();
            _showInSnackBar(state.message);
          }
        },
        child: BlocConsumer<FormsBloc, FormsState>(
          listener: (BuildContext context, FormsState state) {},
          builder: (BuildContext context, state) {
            return Center(
              child: Form(
                  child: ListView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 30),
                    children: [

                      /// Name field
                      RoundedTextField(
                        focusNode: _nameFocusNode,
                        controller: _nameController,
                        hintText: S.of(context).sign_name_hint,
                        validator: (_) {
                          return !state.isUserNameValid ? 'Minimum is 4 characters' : null;
                        },
                      ),
                      const SizedBox(height: 24,),

                      /// Password field
                      RoundedTextField(
                          obscureText: true,
                          controller: _passwordController,
                          hintText: S.of(context).sign_password_hint,
                          validator: (_) {
                            return !state.isPasswordValid ? 'Minimum is 8 characters' : null;
                          }
                      ),
                      const SizedBox(height: 30,),

                      /// Log In Button
                      RoundedAnimatedButton(
                        label: S.of(context).log_in,
                        onPressed: (){
                          _signInCubit.auth(Login(
                            userName: _nameController.text,
                            password: _passwordController.text
                          ));
                        },
                        buttonStatus: _isRegisterButtonEnabled
                            ? ButtonStatus.enabled
                            : ButtonStatus.disabled,
                      ),
                      const SizedBox(height: 24,),

                    ],
              )),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _passwordController.dispose();
    _nameController.dispose();
  }
}
