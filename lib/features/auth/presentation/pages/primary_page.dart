import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kanban_test/features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'package:kanban_test/features/auth/presentation/pages/login_page.dart';
import 'package:kanban_test/features/dashboard/presentation/pages/dashboard_page.dart';

class PrimaryPage extends StatefulWidget {
  const PrimaryPage({Key? key}) : super(key: key);

  @override
  _PrimaryPageState createState() => _PrimaryPageState();
}

class _PrimaryPageState extends State<PrimaryPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(listener: (context, state) async {
      if (state is Authenticated) {
        await Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const DashboardPage()));
      } else if (state is NotAuthenticated) {
        await Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LoginPage()));
      }
    }, builder: (context, state) {
      return const SizedBox();
    });
  }
}
