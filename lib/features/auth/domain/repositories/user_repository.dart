import 'package:dartz/dartz.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/auth/domain/entities/user.dart';

abstract class UserRepository {

  /// returns [void] if successful
  ///
  Future<Either<Failure, bool>> save(User user);

  /// returns [User] if successful
  ///
  Future<Either<Failure, User>> getUser();

  /// remove [User] info
  Future<Either<Failure, bool>> removeUser();

}

