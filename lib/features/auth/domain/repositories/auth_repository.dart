import 'package:dartz/dartz.dart';
import 'package:kanban_test/core/error/failures.dart';
import 'package:kanban_test/features/auth/domain/entities/login.dart';

abstract class AuthRepository {

  Future<Either<Failure, bool>> checkAuth();

  /// [AuthFailure] <-> [String] Token auth access code
  Future<Either<Failure, String>> auth(Login credentials);

  /// [AuthFailure] <-> [bool] result
  Future<Either<Failure, bool>> unAuth();

  Future<Either<Failure, String>> refreshToken();

  Future<Either<Failure, String>> getToken();

  Future<Either<Failure, String>> reAuth();
}
