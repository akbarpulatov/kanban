import 'dart:convert';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));
String userToJson(User data) => json.encode(data.toJson());

@immutable
class User extends Equatable{

  final String userName;
  final String email;
  final String password;

  const User({
    required this.userName,
    required this.email,
    required this.password
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userName: json['username'],
      email: json['email'],
      password: json['password']
    );
  }

  Map<String, dynamic> toJson() => {
    'username': userName,
    'email': email,
    'password': password,
  };

  @override
  List<Object?> get props => [ userName, email, password ];
  
}