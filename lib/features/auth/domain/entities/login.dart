import 'dart:convert';

Login userFromJson(String str) => Login.fromJson(json.decode(str));
String userToJson(Login data) => json.encode(data.toJson());

class Login {
  final String userName;
  final String password;
   
  Login({required this.userName, required this.password});

  factory Login.fromJson(Map<String, dynamic> json) =>
      Login(userName: json['UserName'], password: json['UserPassword']);

  Map<String, dynamic> toJson() => <String, dynamic>{'UserName': userName, 'UserPassword': password};

  Map<String, dynamic> toMap() => <String, dynamic>{'username': userName, 'password': password};
}
