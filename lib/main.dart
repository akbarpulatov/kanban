import 'dart:io';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:kanban_test/features/auth/presentation/manager/forms_bloc/forms_bloc.dart';
import 'package:kanban_test/features/auth/presentation/manager/sign_in_cubit/sign_in_cubit.dart';
import 'package:kanban_test/features/auth/presentation/pages/primary_page.dart';
import 'package:kanban_test/service_locator.dart';
import 'package:kanban_test/theme.dart';
import 'package:path_provider/path_provider.dart';
import 'features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'features/dashboard/data/data_sources/dashboard_local_data_source.dart';
import 'features/dashboard/domain/entities/kanban_card.dart';
import 'generated/l10n.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final Directory directory = await getApplicationDocumentsDirectory();
  Hive.init(directory.path);
  await initServices();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => sl<FormsBloc>()),
        BlocProvider(create: (context) => sl<AuthCubit>()),
        BlocProvider(create: (context) => sl<SignInCubit>()),
      ],
      child: AdaptiveTheme(
        initial: AdaptiveThemeMode.dark,
        light: kLightTheme,
        dark: kDarkTheme,
        builder: (light, dark) => MaterialApp(
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          theme: light,
          darkTheme: dark,
          title: 'Flutter Demo',
          home: const PrimaryPage(),
        ),
      ),
    );
  }
}
