import 'package:flutter/material.dart';

final kDarkTheme = ThemeData.dark().copyWith(
  cardColor: Colors.greenAccent,
  scaffoldBackgroundColor: Colors.black,
);

final kLightTheme = ThemeData.light().copyWith(

);