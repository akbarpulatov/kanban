import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:kanban_test/core/network/client.dart';
import 'package:kanban_test/features/auth/data/data_sources/auth_local_data_source.dart';
import 'package:kanban_test/features/auth/data/data_sources/auth_remote_data_source.dart';
import 'package:kanban_test/features/auth/data/data_sources/user_local_data_source.dart';
import 'package:kanban_test/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:kanban_test/features/auth/data/repositories/user_repository_impl.dart';
import 'package:kanban_test/features/auth/domain/repositories/auth_repository.dart';
import 'package:kanban_test/features/auth/domain/repositories/user_repository.dart';
import 'package:kanban_test/features/auth/presentation/manager/auth_cubit/auth_cubit.dart';
import 'package:kanban_test/features/auth/presentation/manager/forms_bloc/forms_bloc.dart';
import 'package:kanban_test/features/auth/presentation/manager/sign_in_cubit/sign_in_cubit.dart';
import 'package:kanban_test/features/dashboard/data/data_sources/dashboard_local_data_source.dart';
import 'package:kanban_test/features/dashboard/data/data_sources/dashboard_remote_data_source.dart';
import 'package:kanban_test/features/dashboard/data/repositories/dashboard_repository_impl.dart';
import 'package:kanban_test/features/dashboard/presentation/manager/row_items/row_items_cubit.dart';
import 'features/dashboard/domain/repositories/dashboard_repository.dart';

final sl = GetIt.instance;

Future<void> initServices() async {
  /// Core

  /// **************************************************************
  /// Features - Auth
  /// Bloc
  sl.registerLazySingleton(() => FormsBloc());
  sl.registerLazySingleton(() => AuthCubit(authRepository: sl(), signInCubit: sl(), userRepo: sl()));
  sl.registerLazySingleton(() => SignInCubit(sl()));

  /// Repository
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(
        localDataSource: sl(),
        remoteDataSource: sl(),
      ));

  sl.registerLazySingleton<UserRepository>(() => UserRepositoryImpl(localDataSource: sl()));

  /// Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(() => AuthRemoteDataSourceImpl(sl()));
  sl.registerLazySingleton<AuthLocalDataSource>(() => AuthLocalDataSourceImpl(sl()));
  sl.registerLazySingleton<UserLocalDataSource>(() => UserLocalDataSourceImpl(sl()));

  /// **************************************************************
  /// Features - Dashboard
  /// Bloc
  sl.registerFactory(() => RowItemsCubit(sl()));

  /// Repository
  sl.registerLazySingleton<DashboardRepository>(() => DashboardRepositoryImpl(remoteDataSource: sl(), localDataSource: sl()));

  /// DataSources
  sl.registerLazySingleton<DashboardRemoteDataSource>(() => DashboardRemoteDataSourceImpl(sl()));
  sl.registerLazySingleton<DashboardLocalDataSource>(() => DashboardLocalDataSourceImpl(sl()));

  /// External
  final box = await Hive.openBox<String>('storage');
  sl.registerLazySingleton<Box<String>>(() => box);
  sl.registerLazySingleton(() => const FlutterSecureStorage());
  final network = await NetworkClient().init(sl());
  sl.registerLazySingleton(() => network);
}
